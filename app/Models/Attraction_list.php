<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Attraction_list extends Model
{
    use HasFactory;

    protected $table = "attraction_list";
    public $timestamps = false;

    public function allData()
    {
        return DB::table('attraction_list')->get();

    }
}
