<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Attraction extends Model
{
    use HasFactory;

    protected $table = "attraction";
    public $timestamps = false;

    public function index()
    {
        return DB::table('attraction', 'a')
            ->join('attraction_list as al', 'al.attraction_id', '=', 'a.attraction_type')
            ->select('a.*', 'al.attraction_name as attraction', 'al.attraction_price as price')
            ->get();
    }

    public function show($id)
    {
        return DB::table('attraction', 'a')
            ->join('attraction_list as al', 'al.attraction_id', '=', 'a.attraction_type')
            ->select('a.*', 'al.attraction_name as attraction', 'al.attraction_price as price')
            ->where('a.attraction_type', $id)->get();
    }

    public function IDshow($id)
    {
        return DB::table('attraction', 'a')
            ->join('attraction_list as al', 'al.attraction_id', '=', 'a.attraction_type')
            ->select('a.*', 'al.attraction_name as attraction', 'al.attraction_price as price')
            ->where('a.id', $id)->get();
    }
}
