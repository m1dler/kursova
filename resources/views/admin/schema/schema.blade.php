<!doctype html>
<html lang="ua">
<head>
    <title>@yield('title-block')</title>
    <meta charset="utf-8"/>
    <link rel="stylesheet" type="text/css" href="{{URL::asset('css/app.css')}}"/>
</head>
<body>
<p>
<h2>Парк розваг::Admin</h2>
@yield('content')
@if(isset($data))
    <table>
        <thead>
        <tr>
            <th>Ім'я відвідувача</th>
            <th>Атракціон</th>
            <th>Ціна</th>
            <th>Дата</th>
            <th>Дії</th>
        </tr>
        </thead>
        <tbody>
        @foreach ($data as $row)
            <tr>
                <td>{{$row->guest_name}}</td>
                <td>{{ $row->attraction}}</td>
                <td> {{$row->price}} </td>
                <td> {{$row->data}} </td>
                <td>
                    <a href="{{route('admin.edit',['id' => $row->id]) }}">Редагувати</a>
                    <form action="{{ route('admin.destroy' , $row->id)}}" method="POST">
                        <input name="_method" type="hidden" value="DELETE">
                        {{ csrf_field() }}
                        <button type="submit">Видалити</button>
                    </form>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endif
</body>
</html>
