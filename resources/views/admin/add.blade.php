@extends('admin.schema.schema')

@section('title-block','Admin')

@section('content')
    <div>
        <h1>Введіть інформацію про відвідувача</h1>
        <form action="{{route('admin.store')}}" method="post">
            @csrf
            <p>
                <label for="name">Ім'я відвідувача</label>
                <input type="text" placeholder="Ім'я відвідувача" id="name" name="guest_name" required>
            </p>
            <p>
                <label for="attraction_type">Атракціон</label>
                <select id="attraction_type" name="attraction_type" required>
                    @foreach($attractions as $attraction)
                        <option value='{{ $attraction->attraction_id }}'>{{ $attraction->attraction_name }}</option>
                    @endforeach
                </select>
            </p>
            <p>
                <label for="date">Дата</label>
                <input type="date" placeholder="Дата" id="date" name="date" required>
            </p>
            <p>
                <button type="submit">Додати</button>
                <a href="{{route('admin.index')}}">На головну</a>

            </p>
        </form>
    </div>
@endsection
